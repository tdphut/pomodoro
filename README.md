# Pomodoro
Simple Pomodoro Techneque mobile app powered by react native

## Get started
Install [node](https://nodejs.org/en/), [yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable), [expo-cli](https://reactnative.dev/docs/environment-setup) on your local machine.

Clone this project:

`git clone https://github.com/tdphut/Pomodoro.git`

Install dependencies:

`yarn install`

Start development:

`yarn start`

Scan QR code from Metro Bundle or connect to a virtual machine to run the app.

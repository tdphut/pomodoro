import React from 'react'
import {
	Modal,
	StyleSheet,
	Text,
	TouchableHighlight,
	View
} from 'react-native'
import PropTypes from 'prop-types'



const TimerModal = props => {
	const { modalVisible, reset, timerType, handleStartButton } = props
	let message
	switch (timerType) {
	case 'WORK':
		message = 'Work time 🎯'
		break
	case 'SHORT_BREAK':
		message = 'It\'s time to take a short break 😊'
		break
	case 'LONG_BREAK':
		message = 'It\'s a long break time 😀'
		break
	default:
		throw new Error('Unknow timer type!')
	}
	return (
		<Modal
			animationType='slide'
			transparent={true}
			visible={modalVisible}
		>
			<View style={styles.centeredView}>
				<View style={styles.modalView}>
					<Text style={styles.modalText}>{message}</Text>
					<View style={styles.action}>
						<TouchableHighlight
							style={styles.actionButton}
							onPress={() => reset()}
						>
							<Text style={styles.textStyle}>Stop</Text>
						</TouchableHighlight>

						<TouchableHighlight
							style={styles.actionButton}
							onPress={() => {
								handleStartButton()
							}}
						>
							<Text style={styles.textStyle}>Start</Text>
						</TouchableHighlight>
					</View>
				</View>
			</View>
		</Modal>
	)
}

TimerModal.propTypes = {
	reset: PropTypes.func.isRequired,
	handleStartButton: PropTypes.func.isRequired,
	timerType: PropTypes.string.isRequired,
	modalVisible: PropTypes.bool
}

const styles = StyleSheet.create({
	centeredView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	modalView: {
		width: 280,
		backgroundColor: 'white',
		borderRadius: 10,
		paddingTop: 30,
		paddingBottom: 30,
		alignItems: 'center',
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5
	},
	action: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'space-evenly'
	},
	actionButton: {
		backgroundColor: '#2196F3',
		borderRadius: 5,
		padding: 10,
		elevation: 2,
		width: 80,
	},
	textStyle: {
		color: 'white',
		textAlign: 'center',
		fontSize: 18,
	},
	modalText: {
		marginBottom: 30,
		textAlign: 'center',
		fontSize: 18,
		paddingLeft: 20,
		paddingRight: 20,
	}
});

export default TimerModal

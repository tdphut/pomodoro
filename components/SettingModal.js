import React from 'react'
import { View, Modal, StyleSheet, Text, TouchableOpacity, Picker } from 'react-native'
import PropTypes from 'prop-types'
import { AntDesign } from '@expo/vector-icons'

const SECONDS = 60

const SettingModal = props => {
	const {
		showSettingModal, setShowSettingModal, 
		numOfSessions, shortBreakTime, longBreakTime, 
		focusTime, setShortBreakTime, setLongBreakTime, 
		setNumOfSession, setFocusTime, shortBreaks, longBreaks, focus } = props
	const generateSessions = (new Array(numOfSessions)).fill(0).map((v,i) => (i + 1))
	return (
		<Modal 
			animationType='slide'
			transparent={true}
			visible={props.showSettingModal}
		>
			<View style={styles.container}>
				<View style={styles.modalView}>
					<View style={styles.header}>
						<Text style={styles.headerText}>Setting</Text>
						<TouchableOpacity 
							onPress={() => setShowSettingModal(!showSettingModal)}>
							<AntDesign name='close' color='#0000FF' size={22} />
						</TouchableOpacity>
					</View>
					<View style={styles.setting}>
						<View style={styles.settingItem}>
							<Text>Focus in</Text>
							<View style={styles.pickerStyle}>
								<Picker
									style={styles.picker}
									prompt='Focus in'
									selectedValue={focusTime / 60}
									onValueChange={(itemValue,) => setFocusTime(itemValue * SECONDS)}>
									{focus.map(
										(time, index) => 
											<Picker.Item key={index} label={`${time} minutes`} value={time} />)}
								</Picker>
							</View>
						</View>
						<View style={styles.settingItem}>
							<Text>Short break in</Text>
							<View style={styles.pickerStyle}>
								<Picker
									style={styles.picker}
									prompt='Short break in'
									selectedValue={shortBreakTime / 60}
									onValueChange={(itemValue,) => setShortBreakTime(itemValue * SECONDS)}>
									{shortBreaks.map(
										(time, index) => 
											<Picker.Item key={index} label={`${time} minutes`} value={time} />)}
								</Picker>
							</View>
						</View>
						<View style={styles.settingItem}>
							<Text>Long break after</Text>
							<View style={styles.pickerStyle}>
								<Picker
									style={styles.picker}
									prompt='Long break after'
									selectedValue={numOfSessions}
									onValueChange={(itemValue,) => setNumOfSession(itemValue)}>
									{generateSessions.map(
										(session, index) => 
											<Picker.Item key={index} label={`${session} short break`} value={session} />)}
								</Picker>
							</View>
						</View>
						<View style={styles.settingItem}>
							<Text>Long break in</Text>
							<View style={styles.pickerStyle}>
								<Picker
									style={styles.picker}
									prompt='Long break in'
									selectedValue={longBreakTime / 60}
									onValueChange={
										(itemValue,) => setLongBreakTime(itemValue * SECONDS)}>
									{longBreaks.map((time, index) => 
										<Picker.Item key={index} label={`${time} minutes`} value={time} />)}
								</Picker>
							</View>
						</View>
						<View style={styles.settingItem}>
							<TouchableOpacity>
								<Text style={styles.defaultSetting}>Default setting</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</View>
		</Modal>
	)
}

SettingModal.propTypes = {
	showSettingModal: PropTypes.bool.isRequired,
	setShowSettingModal: PropTypes.func.isRequired,
	numOfSessions: PropTypes.number.isRequired,
	longBreakTime: PropTypes.number.isRequired,
	shortBreakTime: PropTypes.number.isRequired,
	shortBreaks: PropTypes.array.isRequired,
	longBreaks: PropTypes.array.isRequired,
	focus: PropTypes.array.isRequired,
	focusTime: PropTypes.number.isRequired,
	setFocusTime: PropTypes.func.isRequired,
	setLongBreakTime: PropTypes.func.isRequired,
	setShortBreakTime: PropTypes.func.isRequired,
	setNumOfSession: PropTypes.func.isRequired,
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	modalView: {
		flex: 0.75,
		width: '100%',
		marginBottom: -10,
		backgroundColor: 'white',
		borderRadius: 10,
		padding: 30,
		alignItems: 'center',
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5
	},
	header: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'baseline',
	},
	headerText: {
		fontSize: 18,
		fontWeight: 'bold'
	},
	setting: {
		width: '100%',
		flexDirection: 'row',
		flexWrap: 'wrap',
		marginTop: 50,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	settingItem: {
		marginTop: 20,
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'flex-end',
	},
	pickerStyle: {
		width: 115,
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: 'grey',
		borderRadius: 6,
		paddingLeft: 5,
		paddingRight: 5,
	},
	defaultSetting: {
		marginTop: 30,
	},
	picker: {
		height: 30,
	},
})

export default SettingModal
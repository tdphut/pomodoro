import React from 'react'
import { AntDesign } from '@expo/vector-icons'
import {
	TouchableOpacity, View
} from 'react-native'
import PropTypes from 'prop-types'

const SettingIcon = props => {
	const {showSettingModal, setShowSettingModal} = props
	return (
		<View>
			<TouchableOpacity onPress={() => setShowSettingModal(!showSettingModal)}>
				<AntDesign name='setting' color='#3d5875' size={30} />
			</TouchableOpacity>
		</View>
	)
}

SettingIcon.propTypes = {
	showSettingModal: PropTypes.bool.isRequired,
	setShowSettingModal: PropTypes.func.isRequired,
}

export default SettingIcon


import React from 'react'
import { StyleSheet, View } from 'react-native'
import PropTypes from 'prop-types'
import { Feather } from '@expo/vector-icons'

const Indicator = props => (
	<View style={styles.indicator}>
		{props.sessions.map((isComplete, index) =>
			<Feather
				color={isComplete ? '#0000FF' : '#3d5875'}
				style={styles.sessionIcon}
				key={index}
				name={isComplete ? 'check-circle' : 'circle'}
				size={25} />)}
	</View>)

Indicator.propTypes = {
	sessions: PropTypes.array.isRequired
}

const styles = StyleSheet.create({
	indicator: {
		flex: 20,
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	},
	sessionIcon: {
		marginLeft: 5,
		marginRight: 5,
	}
});

export default Indicator
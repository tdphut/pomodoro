import React from 'react'
import { TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { Ionicons } from '@expo/vector-icons'

const InfoIcon = ({navigation}) => {
	return (
		<TouchableOpacity onPress={() => navigation.navigate('About')}>
			<Ionicons 
				style={{marginRight: 20}}
				size={25} name='ios-information-circle-outline'
				color='#3d5875' />
		</TouchableOpacity>
	)
}

InfoIcon.propTypes = {
	navigation: PropTypes.object.isRequired,
}

export default InfoIcon
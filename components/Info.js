import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import PropTypes from 'prop-types'

const Info = props => (
	<View style={styles.container}>
		<Text style={styles.text}>{props.text}</Text>
	</View>)

const styles = StyleSheet.create({
	container: {
		flex: 20,
		flexDirection: 'row',
		alignItems: 'center',
	},
	text: {
		fontSize: 18,
	}
})

Info.propTypes = {
	text: PropTypes.string.isRequired,
}

export default Info

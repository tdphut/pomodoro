import React from 'react'
import { Text } from 'react-native'
import { Linking } from 'expo'
import PropTypes from 'prop-types'

export default class Anchor extends React.Component {
	_handlePress = () => {
		Linking.openURL(this.props.href);
		this.props.onPress && this.props.onPress()
	}

	render() {
		return (
			<Text {...this.props} onPress={this._handlePress}>
				{this.props.children}
			</Text>
		)
	}
}

Anchor.propTypes = {
	href: PropTypes.string.isRequired,
	onPress: PropTypes.func,
	children: PropTypes.string
}


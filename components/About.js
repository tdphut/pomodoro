import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'
import Anchor from './Anchor'

const pomodoroUrl = 'https://en.wikipedia.org/wiki/Pomodoro_Technique'
const authorUrl = 'https://tdphut.com'
const authorEmail = 'mailto: tdphut@gmail.com'

const About = () => {
	return (
		<View style={styles.container}>
			<View style={styles.session}>
				<Ionicons name='ios-information-circle' size={30} color='#3d5875' />
				<Text style={styles.header}>Pomodoro Technique</Text> 
				<Anchor href={pomodoroUrl}>
						Learn more about pomodoro technique on wikipidia
				</Anchor>
			</View>
			<View style={styles.line} />
			<View style={styles.session}>
				<Ionicons name='ios-contact' size={30} color='#3d5875' />
				<Text style={styles.header}>Author</Text>
				<Anchor href={authorUrl}>Tran Duy Phut</Anchor>
			</View>
			<View style={styles.line} />
			<View style={styles.session}>
				<MaterialCommunityIcons name='email' size={30} color='#3d5875' />
				<Text style={styles.header}>Contact</Text>
				<Anchor href={authorEmail}>Shoot me an email</Anchor>
			</View>
			<View style={styles.line} />
			<View style={styles.session}>
				<Ionicons name='ios-rocket' size={35} color='#3d5875' />
				<Text style={styles.header}>Version</Text>
				<Text>1.0.0</Text>
			</View>
			<View style={styles.line} />
			<View style={styles.session}>
				<Ionicons name='ios-share-alt' size={35} color='#3d5875' />
				<Text style={styles.header}>Share It</Text>
				<Text>Happiness is sharing</Text>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	session: {
		flex: 0.18,
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		flexWrap: 'wrap',
	},
	header: {
		width: '100%',
		fontSize: 18,
		textAlign: 'center',
		marginBottom: 5,
	},
	textContent: {
		width: '100%',
		textAlign: 'center',
	},
	line: {
		marginTop: 5,
		width: 100,
		borderBottomColor: '#3d5875',
		borderBottomWidth: StyleSheet.hairlineWidth
	}
})

export default About
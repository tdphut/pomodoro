import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import PropTypes from 'prop-types'
import { AnimatedCircularProgress } from 'react-native-circular-progress';

const SECOND = 60

const Timer = props => {
	const minutes = Math.floor(props.secondsLeft / SECOND)
	const seconds = props.secondsLeft % SECOND
	return (
		<View style={styles.container}>
			<AnimatedCircularProgress
				size={230}
				width={4}
				backgroundWidth={2}
				rotation={0}
				fill={props.fill}
				tintColor="blue"
				backgroundColor="#3d5875">
				{
					() => (
						<Text style={styles.timer}>
							{minutes < 10 ? `0${minutes}` : minutes}
            :
							{seconds < 10 ? `0${seconds}` : seconds}
						</Text>
					)
				}
			</AnimatedCircularProgress>
		</View>
	)
}

Timer.propTypes = {
	secondsLeft: PropTypes.number.isRequired,
	fill: PropTypes.number.isRequired,
}

export default Timer

const styles = StyleSheet.create({
	container: {
		flex: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},
	timer: {
		fontSize: 45,
		textAlign: 'center',
	},
});

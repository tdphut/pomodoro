import React from 'react'
import { EvilIcons } from '@expo/vector-icons'
import {
	TouchableOpacity, View
} from 'react-native'
import PropTypes from 'prop-types'

const StartIcon = props => {
	const { start, setStart } = props
	return (
		<View>
			<TouchableOpacity onPress={() => setStart(!start)}>
				<EvilIcons 
					name={start ? 'close-o' : 'play'}
					color='#3d5875' size={80} />
			</TouchableOpacity>
		</View>
	)
}

StartIcon.propTypes = {
	start: PropTypes.bool.isRequired,
	setStart: PropTypes.func.isRequired,
}

export default StartIcon
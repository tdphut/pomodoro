import React from 'react'
import { Entypo } from '@expo/vector-icons'
import {
	TouchableOpacity, View
} from 'react-native'
import PropTypes from 'prop-types'

const LightIcon = props => {
	const { screenOn, toggleScreenOff } = props
	return (
		<View>
			<TouchableOpacity onPress={() => toggleScreenOff(!screenOn)}>
				<Entypo 
					name={screenOn ? 'light-up' : 'light-down'}
					color={screenOn? '#0000FF' : '#3d5875'} size={30} />
			</TouchableOpacity>
		</View>
	)
}

LightIcon.propTypes = {
	screenOn: PropTypes.bool.isRequired,
	toggleScreenOff: PropTypes.func.isRequired,
}

export default LightIcon
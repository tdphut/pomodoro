import 'react-native-gesture-handler'
import React from 'react'
import {
	NavigationContainer
} from '@react-navigation/native'
import {
	StyleSheet, View, AsyncStorage, ToastAndroid, Platform, AppState
} from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { activateKeepAwake, deactivateKeepAwake } from 'expo-keep-awake'
import { Audio } from 'expo-av'
import {Notifications, Permissions} from 'expo'
import Timer from './components/Timer'
import SettingIcon from './components/SettingIcon'
import defaultSetting from './config/index'
import TimerModal from './components/TimerModal'
import Indicator from './components/Indicator'
import StartIcon from './components/StartIcon'
import LightIcon from './components/LightIcon'
import Info from './components/Info'
import SettingModal from './components/SettingModal'
import InfoIcon from './components/InfoIcon'
import About from './components/About'

const Stack = createStackNavigator()
const MILISECOND = 1000
const soundRow = require('./assets/sounds/eventually.mp3')

export default function AppStack() {
	return (
		<NavigationContainer>
			<Stack.Navigator initialRouteName='Home'>
				<Stack.Screen
					name='Home'
					component={Home}
					options={({navigation}) => ({ 
						title: 'Pomodoro',
						headerTitleAlign: 'center',
						headerRight: () => <InfoIcon navigation={navigation} />,
					})} />
				<Stack.Screen
					name='About'
					component={About}
					options={{
						title: 'About',
						headerTitleAlign: 'center',
					}} />
			</Stack.Navigator>
		</NavigationContainer>
	)
}

export class Home extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			...defaultSetting,
			timerType: 'WORK',
			sessionCounter: 0,
			start: false,
			modalVisible: false,
			screenOn: false,
			showSettingModal: false,
			fill: 0,
			secondsLeft: 0,
			appState: AppState.currentState,
		}
		this.handleStartButton = this.handleStartButton.bind(this)
		this.reset = this.reset.bind(this)
		this.tick = this.tick.bind(this)
		this.toggleScreenOff = this.toggleScreenOff.bind(this)
		this.setFocusTime = this.setFocusTime.bind(this)
		this.setLongBreakTime = this.setLongBreakTime.bind(this)
		this.setShortBreakTime = this.setShortBreakTime.bind(this)
		this.setNumOfSession = this.setNumOfSession.bind(this)
		this.setShowSettingModal = this.setShowSettingModal.bind(this)
	}

	setFocusTime(seconds) {
		if (!this.state.start) {
			this.setState({
				focusTime: seconds,
				then: seconds * MILISECOND,
				secondsLeft: seconds,
				timer: seconds
			})
		}
		this.updateSettingToStorage({focusTime: seconds})
	}

	setShortBreakTime(seconds) {
		if (this.state.start) {
			this.setState({
				shortBreakTime: seconds
			})
		}
		this.updateSettingToStorage({shortBreakTime: seconds})
	}

	setLongBreakTime(seconds) {
		if (!this.state.start) {
			this.setState({
				longBreakTime: seconds
			})
		}
		this.updateSettingToStorage({longBreakTime: seconds})
	}

	setNumOfSession(amount) {
		if (!this.state.start) {
			this.setState({
				numOfSessions: amount
			})
		}
		this.updateSettingToStorage({numOfSessions: amount})
	}

	setShowSettingModal(show) {
		this.setState({
			showSettingModal: show
		})
	}

	// Set inititial state
	async reset() {
		clearInterval(this.timerId)
		const setting = await this.readSettingFromStorage()
		this.setState({
			...setting,
			timerType: 'WORK',
			sessionCounter: 0,
			start: false,
			modalVisible: false,
			showSettingModal: false,
			fill: 0,
			then: setting.focusTime * MILISECOND,
			secondsLeft: setting.focusTime,
			timer: setting.focusTime,
		})
	}

	async setLongBreak() {
		await this.reset()
		this.setState({
			modalVisible: true
		})
	}

	setNextSession() {
		clearInterval(this.timerId)
		const sessions = [...this.state.sessions]
		let sessionCounter = this.state.sessionCounter
		let seconds, timerType
		switch (this.state.timerType) {
		case 'WORK':
			if (this.state.sessionCounter == this.state.numOfSessions - 1) {
				seconds = this.state.longBreakTime,
				timerType = 'LONG_BREAK'
			} else {
				seconds = this.state.shortBreakTime,
				timerType = 'SHORT_BREAK'
			}
			sessions[this.state.sessionCounter] = true
			break
		case 'SHORT_BREAK':
			seconds = this.state.focusTime,
			++sessionCounter
			timerType = 'WORK'
			break
		case 'LONG_BREAK':
			this.setLongBreak()
			return
		}

		this.setState({
			modalVisible: true,
			start: false,
			then: seconds * MILISECOND,
			sessionCounter,
			timerType,
			sessions,
			fill: 0,
			secondsLeft: seconds,
			timer: seconds,
		})
	}

	tick() {
		const then = this.state.then + Date.now()
		this.setState({ then })
		this.timerId = setInterval(
			() => {
				const secondsLeft = Math.round((this.state.then - Date.now()) / MILISECOND)
				const fill = 100 - Math.round(secondsLeft * 100 / this.state.timer)
				if (secondsLeft < 0) {
					this.pushLocalNotification()
					this.setNextSession()
				} else {
					this.setState({ secondsLeft, fill })
				}
			},
			MILISECOND
		)
	}

	pushLocalNotification() {
		if (this.state.appState.match(/inactive|background/)) {
			const localNotification = {
				title: 'Ponodoro',
				body: this.getNotifiMessage(),
				ios: { sound: true },
				android: { channelId: 'pomodoro' }
			}
			Notifications.presentLocalNotificationAsync(localNotification)
		} else {
			this.sound.playAsync()
			this.sound.setPositionAsync(0)
		}
	}

	_handleAppStateChange = nextAppState => {
    this.setState({ appState: nextAppState });
  }

	async readSettingFromStorage() {
		try {
			const setting = await AsyncStorage.getItem('setting')
			if (setting) {
				return JSON.parse(setting)
			}
		} catch (error) {
			// TODO: Save log here
		}
		return null
	}

	async updateSettingToStorage(setting) {
		try {
			await AsyncStorage.mergeItem('setting', JSON.stringify(setting))
		} catch (error) {
			// Save log here
		}
	}

	async initStorage() {
		try {
			await AsyncStorage.setItem('setting', JSON.stringify(defaultSetting))
		} catch (error) {
			// Save long here
		}
	}

	async componentDidMount() {
		this.sound = new Audio.Sound()
		this.sound.loadAsync(soundRow)
		//------------------------//
		let setting// = await this.readSettingFromStorage()
		if (setting) {
			this.setState({
				...setting,
				then: setting.focusTime * MILISECOND,
				secondsLeft: setting.focusTime,
				timer: setting.focusTime,
			})
			if (setting.keepAwake) {
				activateKeepAwake()
			}
		} else {
			this.setState({
				...defaultSetting,
				then: defaultSetting.focusTime * MILISECOND,
				secondsLeft: defaultSetting.focusTime,
				timer: defaultSetting.focusTime
			})
			if (defaultSetting.keepAwake) {
				activateKeepAwake()
			}
			await this.initStorage()
		}
		if (Platform.OS == 'ios') {
			await Permissions.askAsync(Permissions.NOTIFICATIONS)
		}
		if (Platform.OS === 'android') {
      Notifications.createChannelAndroidAsync('pomodoro', {
        name: 'pomodoro',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
      });
		}
		AppState.addEventListener('change', this._handleAppStateChange)
	}

	handleStartButton() {
		if (this.state.start) {
			this.reset()
		} else {
			this.setState({ start: true, modalVisible: false })
			this.tick()
		}
	}

	componentWillUnmount() {
		clearInterval(this.timerId)
		AppState.removeEventListener('change', this._handleAppStateChange)
	}

	getCurrentSession() {
		switch (this.state.timerType) {
		case 'WORK':
			return `On session ${this.state.sessionCounter + 1}`
		case 'SHORT_BREAK':
			return 'Short break'
		case 'LONG_BREAK':
			return 'Long break'
		default:
			return ''
		}
	}

	getNotifiMessage() {
		switch (this.state.timerType) {
			case 'WORK':
				return this.state.sessionCounter == this.state.numOfSessions - 1 ? 
				'Take a long break, please!' : 'Short break time, take some rest'
			case 'SHORT_BREAK':
			case 'LONG_BREAK':
				return 'It\'s time to focus on work'
		}
	}

	toggleScreenOff(val) {
		if (val) {
			activateKeepAwake()
			ToastAndroid.show('Screen always on', ToastAndroid.SHORT)
		}
		else {
			deactivateKeepAwake()
		}
		this.setState({
			keepAwake: val,
		})
		this.updateSettingToStorage({keepAwake: val})
	}

	render() {
		return (
			<View style={styles.container}>
				<TimerModal
					handleStartButton={this.handleStartButton}
					timerType={this.state.timerType}
					modalVisible={this.state.modalVisible}
					reset={this.reset} />
				<SettingModal
					showSettingModal={this.state.showSettingModal}
					setShowSettingModal={this.setShowSettingModal}
					focusTime={this.state.focusTime}
					longBreakTime={this.state.longBreakTime}
					shortBreakTime={this.state.shortBreakTime}
					numOfSessions={this.state.numOfSessions}
					setNumOfSession={this.setNumOfSession}
					setLongBreakTime={this.setLongBreakTime}
					setShortBreakTime={this.setShortBreakTime}
					setFocusTime={this.setFocusTime}
					shortBreaks={this.state.shortBreaks}
					longBreaks={this.state.longBreaks}
					focus={this.state.focus} />
				<Info text={this.getCurrentSession()} />
				<Timer fill={this.state.fill} secondsLeft={this.state.secondsLeft} />
				<Indicator sessions={this.state.sessions} />
				<View style={styles.icons}>
					<LightIcon screenOn={this.state.keepAwake} toggleScreenOff={this.toggleScreenOff} />
					<StartIcon start={this.state.start} setStart={this.handleStartButton} />
					<SettingIcon showSettingModal={this.state.showSettingModal} setShowSettingModal={this.setShowSettingModal} />
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontSize: 28,
		fontWeight: '700'
	},
	icons: {
		width: '100%',
		paddingLeft: 30,
		paddingRight: 30,
		flex: 20,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	}
})

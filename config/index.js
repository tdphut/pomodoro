// Time is caculate in second
const SECONDS = 60
const numOfSessions = 4

const defaultSetting = {
	keepAwake: false,
	numOfSessions,
	focusTime: 10,// * SECONDS,
	shortBreakTime: 4,
	longBreakTime: 6,// * SECONDS,
	sessions: new Array(numOfSessions).fill(false),
	focus: new Array(8).fill(5).map((value, index) => value * (index + 1)),
	shortBreaks: [0.5, ...new Array(5).fill(1).map((value, index) => value + index)],
	longBreaks: [3, 5, 7, 10, 15]
}

export default defaultSetting